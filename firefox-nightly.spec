AutoReqProv: no
# Blobs are built by Mozilla, no way to debug or insert build-id
%undefine _missing_build_ids_terminate_build
%global debug_package %{nil}
%global wayland_backend_default 1

##Init variables
%global packver 92.0
%global pre_version a1
%global arch linux64

%global _appdir %{_libdir}/%{name}
%global _distribution_dir %{_appdir}/distribution
%global langpackdir   %{_appdir}/langpacks

%global tarballdir    firefox

%global brand_name Firefox Nightly

##Package Version and Licences
Summary: Mozilla Firefox Nightly Web browser
Name: firefox-nightly
Version: %{packver}
Release: 0a1_%(date +%%y%%m%%d)%{?dist}
License: MPLv1.1 or GPLv2+ or LGPLv2+
Source0: https://download.mozilla.org/?product=firefox-nightly-latest-ssl&os=%{arch}#/%{name}-%{version}.tar.gz
Source2: default-policies.json
Source3: https://hg.mozilla.org/mozilla-central/raw-file/tip/browser/locales/l10n-changesets.json
Source4: https://hg.mozilla.org/mozilla-central/raw-file/tip/taskcluster/docker/firefox-flatpak/extract_locales_from_l10n_json.py
Source5: https://hg.mozilla.org/mozilla-central/raw-file/tip/build/unix/run-mozilla.sh
Source12:       https://src.fedoraproject.org/rpms/firefox/raw/main/f/firefox-redhat-default-prefs.js
Source20:       https://src.fedoraproject.org/rpms/firefox/raw/main/f/firefox.desktop
Source21:       https://src.fedoraproject.org/rpms/firefox/raw/main/f/firefox.sh.in
Source23:       https://src.fedoraproject.org/rpms/firefox/raw/main/f/firefox.1
Source25:       https://src.fedoraproject.org/rpms/firefox/raw/main/f/firefox-symbolic.svg
Source33:       https://src.fedoraproject.org/rpms/firefox/raw/main/f/firefox.appdata.xml.in
Source34:       https://src.fedoraproject.org/rpms/firefox/raw/main/f/firefox-search-provider.ini


Group: Applications/Internet
Provides:       webclient
URL: https://www.mozilla.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

Requires: alsa-lib libX11 libXcomposite libXdamage libnotify libXt libXext glib2 dbus-glib libjpeg-turbo cairo-gobject libffi fontconfig freetype libgcc gtk3 gtk2 hunspell zlib
Requires: nspr >= 4.10.8
Requires: nss >= 3.19.2
Requires: sqlite >= 3.8.10.2

%if 0%{?fedora} > 31
Recommends:     mozilla-openh264 >= 2.1.1
%endif

BuildRequires: python3
BuildRequires: curl
BuildRequires: desktop-file-utils

##Description for Package

%description
This package is a package built directly from Mozilla's nightly tarball. This package will be updated weekly if not sooner.

%prep
%autosetup -n %{tarballdir}


%build
## Firefox explicitly supports python3, so its also our choice of interpreter. Note that there is no dependency on python.
## These scripts are only used when generating debug information in case of a crash, so not critical to the operation of firefox.
find %{_builddir} -name '*.py' -type f -exec sed -i -e 's,#!/usr/bin/python,#!/usr/bin/python3,' -e 's,/usr/bin/env python,/usr/bin/env python3,' -s {} \;

# ## Language pack
# locales=$(python3 %{SOURCE4} %{SOURCE3})
# %{__mkdir_p} %{buildroot}%{_distribution_dir}/extensions
# for locale in $locales; do
#     curl -o "%{buildroot}%{_distribution_dir}/extensions/langpack-${locale}@firefox.mozilla.org.xpi" \
#         "https://download-installer.cdn.mozilla.net/pub/firefox/nightly/latest-mozilla-central-l10n/linux-x86_64/xpi/firefox-%{version}%{?pre_version}.${locale}.langpack.xpi"
# done

## Install Instructions
%install
%{__install} -dm 755 %{buildroot}/usr/{bin,share/{applications,icons/hicolor/128x128/apps},opt}
%{__install} -dm 755 %{buildroot}%{_appdir}/browser/defaults/preferences/

# Desktop entry
%{__sed} -i -e "s,Exec=firefox ,Exec=%{name} --name %{name} ,g" \
            -e "s,Icon=firefox,Icon=%{name},g" \
            -e "s,Name=Firefox,Name=%{brand_name},g" %{SOURCE20}
mv %{SOURCE20} %{name}.desktop
desktop-file-install --dir %{buildroot}/%{_datadir}/applications %{name}.desktop

for s in 16 32 48 64 128; do
    %{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps
    %{__cp} -p %{_builddir}/firefox/browser/chrome/icons/default/default${s}.png \
               %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps/%{name}.png
done

# Install hight contrast icon
%{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/symbolic/apps
%{__cp} -p %{SOURCE25} \
           %{buildroot}%{_datadir}/icons/hicolor/symbolic/apps/%{name}-symbolic.svg

# Blobs provided by Mozilla
%{__cp} -rf %{_builddir}/firefox/* %{buildroot}%{_appdir}

# set up the firefox start script
%if 0%{?wayland_backend_default}
%global wayland_default true
%else
%global wayland_default false
%endif
%{__rm} -rf %{buildroot}%{_bindir}/%{name}
%{__sed} -e 's/__DEFAULT_WAYLAND__/%{wayland_default}/' \
          -e "s,\$MOZ_LIB_DIR/firefox/,$MOZ_LIB_DIR/%{name}/,g" \
          -e "s,\$SECONDARY_LIB_DIR/firefox/,$MOZ_LIB_DIR/%{name}/,g" \
          -e "s,/__PREFIX__/bin/firefox,/__PREFIX__/bin/%{name},g" \
         -e 's,/__PREFIX__,%{_prefix},g' %{SOURCE21} > %{buildroot}%{_bindir}/%{name}
%{__chmod} 755 %{buildroot}%{_bindir}/%{name}

## Disable Update Alert
%{__mkdir_p} %{buildroot}%{_distribution_dir}
%{__install} -m644 %{SOURCE2} %{buildroot}%{_distribution_dir}/policies.json

# Default
%{__cp} %{SOURCE12} %{buildroot}%{_appdir}/browser/defaults/preferences/

# Copy over run-mozilla.sh
%{__install} -m755 %{SOURCE5} %{buildroot}%{_appdir}

# Install Gnome search provider files
mkdir -p %{buildroot}%{_datadir}/gnome-shell/search-providers
%{__sed} -e 's/firefox.desktop/%{name}.desktop/' \
          %{SOURCE34} > %{buildroot}%{_datadir}/gnome-shell/search-providers/%{name}-search-provider.ini

# Install appdata file
mkdir -p %{buildroot}%{_datadir}/metainfo
%{__sed} -e "s/__VERSION__/%{version}/" \
         -e "s/__DATE__/$(date '+%F')/" \
         -e 's/firefox.desktop/%{name}.desktop/g' \
          -e "s,<translation type="gettext">firefox</translation>,<translation type="gettext">%{name}</translation>,g" \
         %{SOURCE33} > %{buildroot}%{_datadir}/metainfo/%{name}.appdata.xml

# Use the system hunspell dictionaries
%{__rm} -rf %{buildroot}%{_appdir}/dictionaries
ln -s %{_datadir}/myspell %{buildroot}%{_appdir}/dictionaries

%{__install} -p -D -m 644 %{SOURCE23} %{buildroot}%{_mandir}/man1/firefox.1

%{__rm} -f %{buildroot}/%{_appdir}/firefox-config
%{__rm} -f %{buildroot}/%{_appdir}/update-settings.ini

# %{__install} -dm 755 %{buildroot}%{_distribution_dir}
%postun
update-desktop-database &> /dev/null || :
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

# Moves defaults/preferences to browser/defaults/preferences
%pretrans -p <lua>
require 'posix'
require 'os'
if (posix.stat("%{_appdir}/browser/defaults/preferences", "type") == "link") then
  posix.unlink("%{_appdir}/browser/defaults/preferences")
  posix.mkdir("%{_appdir}/browser/defaults/preferences")
  if (posix.stat("%{_appdir}/defaults/preferences", "type") == "directory") then
    for i,filename in pairs(posix.dir("%{_appdir}/defaults/preferences")) do
      os.rename("%{_appdir}/defaults/preferences/"..filename, "%{_appdir}/browser/defaults/preferences/"..filename)
    end
    f = io.open("%{_appdir}/defaults/preferences/README","w")
    if f then
      f:write("Content of this directory has been moved to %{_appdir}/browser/defaults/preferences.")
      f:close()
    end
  end
end

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%preun
# is it a final removal?
if [ $1 -eq 0 ]; then
  %{__rm} -rf %{_appdir}/components
  %{__rm} -rf %{_appdir}/extensions
  %{__rm} -rf %{_appdir}/plugins
  %{__rm} -rf %{langpackdir}
fi

##Cleanup
%clean
rm -rf %{buildroot}%{_libdir}/.build-id

##Installed Files
%files
%{_bindir}/%{name}
%{_datadir}/applications/firefox-nightly.desktop
%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
%{_appdir}/
%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
%{_datadir}/icons/hicolor/symbolic/apps/%{name}-symbolic.svg
%{_datadir}/gnome-shell/search-providers/*.ini
%{_datadir}/metainfo/*.appdata.xml
%doc %{_mandir}/man1/*
